#!/usr/bin/env python
from pymongo import MongoClient
import pymongo.errors
import pickle
 
from datetime import datetime
# -*- coding: utf-8 -*-
import sys
import os
import time


import sys

class ProgressBar:
    '''
    Progress bar
    '''
    def __init__ (self, valmax, maxbar, title):
        if valmax == 0:  valmax = 1
        if maxbar > 200: maxbar = 200
        self.valmax = valmax
        self.maxbar = maxbar
        self.title  = title

    def update(self, val):

        # format
        if val > self.valmax: val = self.valmax

        # process
        perc  = round((float(val) / float(self.valmax)) * 100)
        scale = 100.0 / float(self.maxbar)
        bar   = int(perc / scale)

        # render
        out = '\r%20s [%s%s] %3d %% ' % (self.title, '=' * bar, ' ' * (self.maxbar - bar), perc)
        sys.stdout.write(out)
        #Extinction du curseur
        os.system('setterm -cursor off')
        #Rafraichissement de la barre
        sys.stdout.flush()



if(len(sys.argv) == 2):
    if(sys.argv[1] == "Show"):
        try:
            username, password, host, dbname = 'Mounir', '123=-0', '127.0.0.1', 'admin'
            client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
            for data in client['local']['state'].find({"new": {"$eq":"yes"}}):
                print ">>> ***********Start at : " + str(datetime.fromtimestamp(float(data["_id"])))
                for i in range(0,int(data["IterX"])):
                     print ">>> Iteration " + str(i) + " finish at : " + data["Iteration"+str(i)]["TimeS"]
                     print("")
        except pymongo.errors.OperationFailure as e:
            print("ERROR: %s" % (e))
    if(sys.argv[1] == "Progress"):
        try:
            username, password, host, dbname = 'Mounir', '123=-0', '127.0.0.1', 'admin'
            client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
            barre = ProgressBar(100,100, "Progression")
            print'\n'
            i=0
            for data in client['local']['state'].find({"new": {"$eq":"yes"}}):
                i=int(data["IterX"])
            while(True):
                for data in client['local']['state'].find({"new": {"$eq":"yes"}}):
                    #i = i+1
                    i=int(data["IterX"])
                    barre.update(i)
                    time.sleep(10)
                    #os.system('setterm -cursor on')
                    #print'\n'
        except pymongo.errors.OperationFailure as e:
            print("ERROR: %s" % (e))

if(len(sys.argv) == 3):
    if(sys.argv[1] == "restart"):
        print (">>> ***OKK Restart done***")
