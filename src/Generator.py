#!/usr/bin/env python
import sys
sys.path.append('utils')
import GeneratorO as Go
import numpy
import json

import bson
import pickle
import time
from datetime import datetime,date
from pymongo import MongoClient
import pymongo.errors
#
json_data=open("Json.json").read()
data = json.loads(json_data)
print ("Initialisation : ")
a=Go.Generator( float(data["Iteration0"]["xa"]),
                float(data["Iteration0"]["xb"]),
                float(data["Iteration0"]["ya"]),
                float(data["Iteration0"]["yb"]),
                float(data["Iteration0"]["StepX"]),
                float(data["Iteration0"]["StepY"]),
                float(data["Iteration0"]["StepT"]),
                #int(data["Iteration0"]["V"]),
                bool(data["Iteration0"]["Gaus"])
 
                )
#
a.Solve()
mat = bson.binary.Binary(pickle.dumps(a.fxy, protocol = 2))

data["Iteration0"]["Z"]=mat
#print pickle.loads(data["Iteration0"]["Z"])
#m
data["_id"]=str(time.time())
data["new"]=str("yes")
data["IterX"]=str("0")
data["Iteration0"]["TimeS"]=str(datetime.now())

#v
av = numpy.zeros((201,201))
v = bson.binary.Binary(pickle.dumps(av, protocol = 2))

data["Iteration0"]["V"]=v

username, password, host, dbname = 'Mounir', '123=-0', '127.0.0.1', 'admin'
client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
try:
  db = client['local']
  state = db['state']
  state.insert(data)
  for d in state.find(): print(str(d["_id"]))
  print time.time()
except pymongo.errors.OperationFailure as e:
  print("ERROR: %s" % (e))

