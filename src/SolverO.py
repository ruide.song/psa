#:syntax on 
import sys
sys.path.append('utils')

import Solve as So 
import pickle as pk

import numpy
SoO = So.Solve()

from pylab import *
from mpl_toolkits.mplot3d import Axes3D
from pyevtk.hl import gridToVTK


class SolverO:

	def __init__(self,fxy,v,stepX,stepY,stepT,start):
		#SolverO = pk.load(open('FileSave/SaveGeneratorFile','rb'))
		self.Z=fxy
		self.V=v
		self.stepX=stepX
		self.stepY=stepY
		self.stepT=stepT
		self.count=start
		self.norm=np.linalg.norm(self.Z,ord=1)

	def Solve(self):
		#self.SaveImage(1)
                SoO = So.Solve()
		for j in range(self.count*100,(self.count*100)+100):
		    self.Z = SoO.getNext_ftc(numpy.array(self.Z, order = 'F',dtype="complex128"),numpy.array(self.V, order ='F'),self.stepX,self.stepY,self.stepT)
		    #print self.normalize(self.Z)
		    #self.Plot()
		    self.SaveImage(j)	
		print("iteration: " + str(self.count))	
	def SaveImage(self,vt):
		nx = 201
		ny = 201
		x = np.linspace(-10,10.1, nx, dtype=np.float32)
		y = np.linspace(-10, 10.1, ny, dtype=np.float32)
		z = np.linspace(0.0, 1.0, 1, dtype=np.float32)
		N = np.abs(self.Z)
  		filename = 'FileImage/anim_%04d' % vt
  		gridToVTK(filename, x,y, z, pointData = {'N': N.astype(np.float32).reshape((nx, ny, 1), order = 'C')})
	def Plot(self):
		fig = figure()
		ax = Axes3D(fig)
		X = self.X
		Y = self.Y
		X, Y = np.meshgrid(X, Y)
		Z = np.abs(self.Z)/self.norm
		ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='hot')
		show()
	def normalize(self, fxy):
		F = fxy * fxy.conjugate()
                val = F.sum() * self.stepX * self.stepY
		return val

