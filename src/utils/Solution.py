import numpy as np
import math

def factorielle(num):
    f=1
    for i in range(1,num+1):
        f = f*i
    return f

def hermite(x,n):
    if n==0:
        return 1
    elif n==1:
        return 2*x
    else:
        return 2*x*hermite(x,n-1)-2*(n-1)*hermite(x,n-2)


def solution(u,n):
    m=1.0
    w=1.0
    h=1.0
    pi=math.pi
    F1=1.0/math.sqrt(factorielle(n)*pow(2.0,n)) 
    F2=pow(m*w/(pi*h),0.25)
    F3= np.e**(-(m*w*pow(u,2.0))/(2.0*h)) 
    F=F1*F2*F3*hermite(u*math.sqrt(m*w/h),n)
    return F

# Gaussian fonction (2D)
def Gaussian(x, y, A=1.0, sigx=1.0, sigy=1.0, x0=0.0, y0=0.0):     
    F1=np.e**(-pow(y-y0,2)/(2*sigy**2))
    F2=np.e**(-pow(x-x0,2)/(2*sigx**2))
    F3=A*F1*F2   
    return F3

def V_fonc(x, y):
    m=1.0
    w=1.0
    F=0.5*m*w**2*(x**2 + y**2)
    return F;


a = input("input the range of x and y ([a,b]), a: ")
b = input("input the range of x and y ([a,b]), b: ")
step = input("input the step of x and y ([a,b]) : ")
n = input("input nu : ")

init_x = np.arange(a,b,step)
init_y = np.arange(a,b,step)
size = len(init_x)


print("The init  is :")
print init_x

V = np.zeros((size,size))
for i in range (0, size):
    for j in range (0, size):
        V[i][j]=V_fonc(init_x[i], init_y[j])
     
print("The solution of V is :")
print V

fx = solution(init_x,n)
print("The solution of x is :")
print fx
fy = solution(init_y,n)
print("The solution of y is :" )
print fy

fx.shape=(size,1)
fxy= fx * fy
print("The solution of xy is :")
print fxy

init_x.shape= (size,1)
f= Gaussian(init_x, init_y)
print("The solution of Gaussian is :")
print f




