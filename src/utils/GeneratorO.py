import numpy as np
import math
import pickle


class Generator:

	def __init__(self,xa,xb,ya,yb,stepX,stepY,stepT,n,Gaus=0):
		self.xa=xa
		self.xb=xb
		self.ya=ya
		self.yb=yb
		self.stepX=stepX
		self.stepY=stepY
		self.stepT=stepT
		self.n=n
		self.Gaus=Gaus
		self.fxy=[]

	def factorielle(self,num):
    		f=1
    		for i in range(1,num+1):
        		f = f*i
    		return f

	def hermite(self,x,n):
    		if n==0:
        		return 1
    		elif n==1:
        		return 2*x
    		else:
        		return 2*x*self.hermite(x,n-1)-2*(n-1)*self.hermite(x,n-2)


	def solution(self,u,n):
    		m=939.5654133
    		w=1.0
    		h=6.582119514
    		pi=math.pi
    		F1=1.0/math.sqrt(self.factorielle(n)*pow(2.0,n)) 
    		F2=pow(m*w/(pi*h),0.25)
    		F3= np.e**(-(m*w*pow(u,2.0))/(2.0*h)) 
    		F=F1*F2*F3*self.hermite(u*math.sqrt(m*w/h),n)
    		return F


	# Gaussian fonction (2D)
	def Gaussian(self,x, y, A=1.0, sigx=1.0, sigy=1.0, x0=0.0, y0=0.0):     
    		F1=np.e**(-pow(y-y0,2)/(2*sigy**2))
    		F2=np.e**(-pow(x-x0,2)/(2*sigx**2))
    		F3=A*F1*F2   
    		return F3

	def V_fonc(self,x,y):
    		m=939.5654133
    		w=1.0
    		F=0.5*m*w**2*(x**2 + y**2)
    		return F
	
	def Solve(self):
                init_x = np.arange(self.xa,self.xb,self.stepX, dtype=np.float32)
                init_y = np.arange(self.ya,self.yb,self.stepY, dtype=np.float32)
		self.ix= init_x
		self.iy= init_y
                size = len(init_x)
                V = np.zeros((size,size))
                #for i in range (0, size):
                #      for j in range (0, size):
               	#                V[i][j]=self.V_fonc(init_x[i], init_y[j])
                self.v=V
                fx = self.solution(init_x,self.n)
                fy = self.solution(init_y,self.n)
                if self.Gaus == 1:
                        init_x.shape= (size,1)
                        fxy= self.Gaussian(init_x, init_y)
                if self.Gaus == 0:
                        fx.shape=(size,1)
                        fxy=fx * fy
		self.fxy =np.complex_(fxy, order = 'F')
	def saveG(self):
		file_Name = "FileSave/SaveGeneratorFile"
		fileObject = open(file_Name,'wb')
		pickle.dump(self,fileObject)
		fileObject.close()
