#ifndef SOLVE_H
#define SOLVE_H

#include <armadillo>
#include <complex.h>
#include <math.h>

using namespace std;

class Solve
{
  	public:
  	Solve(void);
	arma::cx_mat getNext_ftc(arma::cx_mat, arma::mat, double, double ,double);
	arma::cx_mat getNext_btc(arma::cx_mat, arma::mat, double , double , double);
  	arma::cx_mat getNext_ctc(arma::cx_mat, arma::mat, double , double , double);
  	double h, m ;
	arma::cx_mat c_btc;
	arma::cx_mat c_ctc;
};

#endif
