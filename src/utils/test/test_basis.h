
#include <cxxtest/TestSuite.h>
#include <armadillo>
#include <complex.h>
#include "../Solve.h"

using namespace std;

class  TestBasis : public CxxTest::TestSuite 
{
 public:
  void test1(void)
  {
	Solve solve;
	arma::cx_mat x= arma::randu<arma::cx_mat>(10, 10)*10;
	double n,m ;
	arma::mat V;
	V.zeros(10, 10);
	for (int i=0;i<50; ++i)
	{	
		arma::cx_mat y = x;
		x = solve.getNext_ftc(x,V,0.1,0.1,0.1);
		m=  arma::accu(arma::real(y % arma::conj(y))) * 0.1 *0.1;
		n= arma::accu(arma::real(x % arma::conj(x))) * 0.1 *0.1;
		TS_ASSERT_DELTA(m, n, 3.5e-0);
	}
  }

  void test2(void)
  {
	Solve solve;
	arma::cx_mat x= arma::randu<arma::cx_mat>(10, 10)*10;
	double n,m ;
	arma::mat V;
	V.zeros(10, 10);
	for (int i=0;i<50; ++i)
	{	
		arma::cx_mat y = x;
		x = solve.getNext_btc(x,V,0.1,0.1,0.1);
		m=  arma::accu(arma::real(y % arma::conj(y))) * 0.1 *0.1;
		n= arma::accu(arma::real(x % arma::conj(x))) * 0.1 *0.1;
		TS_ASSERT_DELTA(m, n, 5e-01);
	}
  }

  void test3(void)
  {
	Solve solve;
	arma::cx_mat x= arma::randu<arma::cx_mat>(10, 10)*10;
	double n,m ;
	arma::mat V;
	V.zeros(10, 10);
	for (int i=0;i<50; ++i)
	{	
		arma::cx_mat y = x;
		x = solve.getNext_ctc(x,V,0.1,0.1,0.1);
		m=  arma::accu(arma::real(y % arma::conj(y))) * 0.1 *0.1;
		n= arma::accu(arma::real(x % arma::conj(x))) * 0.1 *0.1;
		TS_ASSERT_DELTA(m, n, 1e-10);
	}
  }

  



};
	
