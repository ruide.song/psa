/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ErrorPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ErrorPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ErrorPrinter >( tmp, argc, argv );
    return status;
}
bool suite_TestBasis_init = false;
#include "test_basis.h"

static TestBasis suite_TestBasis;

static CxxTest::List Tests_TestBasis = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_TestBasis( "test_basis.h", 9, "TestBasis", suite_TestBasis, Tests_TestBasis );

static class TestDescription_suite_TestBasis_test1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_TestBasis_test1() : CxxTest::RealTestDescription( Tests_TestBasis, suiteDescription_TestBasis, 12, "test1" ) {}
 void runTest() { suite_TestBasis.test1(); }
} testDescription_suite_TestBasis_test1;

static class TestDescription_suite_TestBasis_test2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_TestBasis_test2() : CxxTest::RealTestDescription( Tests_TestBasis, suiteDescription_TestBasis, 29, "test2" ) {}
 void runTest() { suite_TestBasis.test2(); }
} testDescription_suite_TestBasis_test2;

static class TestDescription_suite_TestBasis_test3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_TestBasis_test3() : CxxTest::RealTestDescription( Tests_TestBasis, suiteDescription_TestBasis, 46, "test3" ) {}
 void runTest() { suite_TestBasis.test3(); }
} testDescription_suite_TestBasis_test3;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
