#include "Solve.h"

Solve::Solve(void)
{
	h= 6.582119514;
	m= 939.5654133;
}


arma::cx_mat Solve::getNext_ftc(arma::cx_mat psi, arma::mat V, double h_x, double h_y, double h_t)
{	  
	complex<double> i(0.0,1.0); 

	int nbx = psi.n_rows;
	int nby = psi.n_cols;
        
	arma::cx_mat next ;
	next.zeros(nbx+2, nby+2);
	next.submat(1,1,nbx,nby) = psi;
	arma::cx_mat ori = psi; 
	psi += i*h*h_t* ( (next.submat(2,1,nbx+1,nby)+next.submat(0,1,nbx-1,nby)-2.0*psi)/(h_x*h_x)+(next.submat(1,2,nbx,nby+1)+next.submat(1,0,nbx,nby-1)-2.0*psi)/(h_y*h_y) )/(2*m);
	psi +=-i*h_t*ori/h % V;

  	return psi;
}

arma::cx_mat Solve::getNext_btc(arma::cx_mat psi, arma::mat V,double h_x, double h_y, double h_t)
{
	complex<double> i(0.0,1.0); 
	c_btc = -V + i*h/h_t - h*h/m * (1/(h_x*h_x)+1/(h_y*h_y));
	int nbx = psi.n_rows;
	int nby = psi.n_cols;
        
	arma::cx_mat now, next;
	now.zeros(nbx+2, nby+2);
	next.zeros(nbx,nby);
	next =psi;
	psi *= i*h/h_t;
	for (int j =0; j< 50; ++j){
		now.submat(1,1,nbx,nby) = next;	
		next = -h*h/(2*m)*((now.submat(2,1,nbx+1,nby)+now.submat(0,1,nbx-1,nby))/(h_x*h_x) + (now.submat(1,2,nbx,nby+1)+now.submat(1,0,nbx,nby-1))/(h_y*h_y));  
		next += psi;
		next/=c_btc;
	}
  	return next;
}


arma::cx_mat Solve::getNext_ctc(arma::cx_mat psi, arma::mat V,double h_x, double h_y, double h_t)
{
	complex<double> i(0.0,1.0); 
	c_ctc = -V + 2.0*i*h/h_t - h*h/m * ( 1/(h_x*h_x)+1/(h_y*h_y) );
	int nbx = psi.n_rows;
	int nby = psi.n_cols;
        
	arma::cx_mat now, now_2, next;
	now.zeros(nbx+2, nby+2);
	next =psi;
	now_2.zeros(nbx+2, nby+2);
	now_2.submat(1,1,nbx,nby)  =psi;
	for (int j =0; j< 50; ++j){
		now.submat(1,1,nbx,nby) = next;	
		next = -h*h/(m*2)*( (now.submat(2,1,nbx+1,nby)+now.submat(0,1,nbx-1,nby))/(h_x*h_x) + (now.submat(1,2,nbx,nby+1)+now.submat(1,0,nbx,nby-1))/(h_y*h_y) + (now_2.submat(2,1,nbx+1,nby)+now_2.submat(0,1,nbx-1,nby))/(h_x*h_x) + (now_2.submat(1,2,nbx,nby+1)+now_2.submat(1,0,nbx,nby-1))/(h_y*h_y) );  
		next += ( V + i*h*2.0/h_t + h*h/m * (1/(h_x*h_x)+1/(h_y*h_y)) ) % psi;
		next/=c_ctc;
	}
  	return next;
}




