from setuptools import setup, Extension

module1 = Extension('_Solve',
                    include_dirs = ['./include/armanpy/'],
                    libraries = ['m', 'z', 'armadillo'],
                    sources = ['Solve.i', 'Solve.cpp'],
                    swig_opts = ["-c++", "-Wall", "-I.", "-I./include/armanpy/"])

setup (name = 'package_Slove',
       py_modules = ['Solve'],
       version = '1.0',
       description = 'This is a test package',
       ext_modules = [module1])

