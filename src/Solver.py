#!/usr/bin/env python
import sys
sys.path.append('utils')
import GeneratorO as Go
import numpy
import json
import SolverO as So
import bson
import pickle
import time
from datetime import datetime,date
from pymongo import MongoClient
import pymongo.errors



try:
    username, password, host, dbname = 'Mounir', '123=-0', '127.0.0.1', 'admin'
    client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))

    for data in client['local']['state'].find({"new": {"$eq":"yes"}}):
        it = "Iteration" + data["IterX"]
        #m
        bindat = data[it]["Z"]
        data_id = data["_id"]
        print data["_id"]
        mat = pickle.loads(bindat)
        #v
        bindv = data[it]["V"]
        v = pickle.loads(bindv)
        #v = numpy.zeros((201,201))
        S=So.SolverO(mat,v,float(data[it]["StepX"]),float(data[it]["StepY"]),float(data[it]["StepT"]),int(data["IterX"]))
        S.Solve()
        res = bson.binary.Binary(pickle.dumps(S.Z, protocol = 2)) 
        resv = bson.binary.Binary(pickle.dumps(S.V, protocol = 2)) 
        c={
  "Iteration"+ str(int (data["IterX"])+1): {
    "Z":res,
    "norm":"",
    "xa":str(data[it]["xa"]),
    "xb":str(data[it]["xb"]),
    "ya":str(data[it]["ya"]),
    "yb":str(data[it]["yb"]),
    "StepX":str(data[it]["StepX"]),
    "StepY":str(data[it]["StepY"]),
    "StepT":str(data[it]["StepT"]),
    "V":resv,
    "Gaus":"1",
    "TimeS":str(datetime.now())
  }
}
        client['local']['state'].update({"_id": data_id}, {"$set": c})
        client['local']['state'].update({"_id": data_id}, {"$set": {"IterX": str(int (data["IterX"])+1)}})
        break
except pymongo.errors.OperationFailure as e:
    print("ERROR: %s" % (e))

